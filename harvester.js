/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('harvester'); // -> 'a thing'
 */
 module.exports = function(creep)
 {
	creep.memory.lastPosition = [creep.pos.x, creep.pos.y];
    if (creep.energy < creep.energyCapacity && creep.memory.ticksAtTheSamePosition < 40)
    {
        var source = creep.room.find(Game.SOURCES);
        creep.moveTo(source[0]);
        creep.harvest(source[0]);
    }
    else
    {
        creep.moveTo(Game.spawns.Spawn1);
        creep.transferEnergy(Game.spawns.Spawn1);
    }
	
	if (creep.memory.lastPosition == [creep.pos.x, creep.pos.y])
		creep.memory.ticksAtTheSamePosition++;
	else
		creep.memory.ticksAtTheSamePosition = 0;
	
 };