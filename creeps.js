var spawn = Game.spawns.Spawn1;

module.exports = 
{
	builder: function(postfix)
	{
		spawn.createCreep([Game.MOVE,Game.MOVE,Game.CARRY,Game.WORK], "builder" + postfix, {role: "builder"});
	},
	soldier: function(postfix)
	{
		spawn.createCreep([Game.MOVE,Game.MOVE,Game.ATTACK,Game.ATTACK], "soldier" + postfix, {role: "soldier"});
	},
	harvester: function(postfix)
	{
		spawn.createCreep([Game.MOVE,Game.CARRY,Game.CARRY,Game.WORK,Game.WORK], "harvester" + postfix, {role: "harvester", ticksAtTheSamePosition: 0, lastPosition: 0} );
	}
};