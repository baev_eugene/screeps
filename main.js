var harvester = require('harvester');
var builder = require('builder');
var soldier = require('soldier');

var _ = require('lodash');

var creepBuilder = require('creeps');

Memory.Once = false;
Memory.InitialQueueBuilt = false;

//JSON.stringify(Game.creeps["harvester3"].pos.findPathTo(29,11, {maxOps: 200}))

function intDiv(x,y)
{
	return x/y>>0;
}

var creepQueue = [];

Game.init = function()
{
	creepQueue = [	creepBuilder.harvester,
					creepBuilder.harvester,
					creepBuilder.harvester,
					creepBuilder.harvester,
					creepBuilder.soldier,
					creepBuilder.soldier ];


    Memory.config = {
		soldiersCount : 2,
		buildersCount : 2,
		harvestersCount: 4
    };
	
	Memory.info = {
		soldiersCount : 0,
		buildersCount : 0,
		harvestersCount: 0
	}
	
    Memory.Once = true;
};

function countCreeps()
{
	Memory.info.soldiersCount = 0;
	Memory.info.total = 0;
	Memory.info.buildersCount = 0;
	Memory.info.harvestersCount = 0;
	
	console.log("test");
	
	for (var creepName in Game.creeps)
	{
		switch(Game.creeps[creepName].memory.role)
		{
			case "harvester":
				Memory.info.harvestersCount++;
				break;
			case "soldier":
				Memory.info.soldiersCount++;
				break;
			case "builder":
				Memory.info.buildersCount++;
				break;
		}	
		
		Memory.info.total++;
	}
	
}

function main()
{
	Memory.test = {test:0};

    if (!Game.spawns.Spawn1)
    {
        console.log("No spawns detected;");
        return;
    }
    
	if (Memory.InitialQueueBuilt)
		createCreepIfNeeded();
	else
	{
		if (creepQueue.length)
		{
			var func = creepQueue.pop();
			func();
		}
		else
		{
			InitialQueueBuilt = true;
		}
		
	}

	if ((Game.time % 5) === 0)
	{
		countCreeps();
	}
	
    var cname;

    for (cname in Game.creeps)
    {
	    createCreepIfNeeded();

        var creep = Game.creeps[cname];
    
        if (creep.memory.role == "builder")
        {
            builder(creep);
        }
        else if(creep.memory.role == "harvester") 
        {
            harvester(creep);
        }
        else if(creep.memory.role == "soldier")
        {
            soldier(creep);
        }
    }
}

function createCreepIfNeeded()
{
	
	var spawn = Game.spawns.Spawn1;
	
	if (spawn.spawning)
		return;
		
	if (Memory.info.harvestersCount < Memory.config.harvestersCount)
	{
		Memory.info.harvestersCount++;
		creepBuilder.harvester(Memory.info.harvestersCount);
		return;
	}

	if ((Memory.info.soldiersCount < Memory.config.soldiersCount))
	{
		Memory.info.soldiersCount++;
		creepBuilder.soldier(Memory.info.soldiersCount);
		return;
	}

	if (Memory.info.buildersCount < Memory.config.buildersCount)
	{
		Memory.info.buildersCount++;
		creepBuilder.builder(Memory.info.buildersCount);
		return;
	}
	

}


if (!Memory.Once)
    Game.init();
    
main();
