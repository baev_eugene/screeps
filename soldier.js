module.exports = function(creep)
{
	var target = creep.pos.findNearest(Game.HOSTILE_CREEPS);
	var spawn = Game.spawns.Spawn1;
	
	if (creep.energy === 0)
	{
		creep.moveTo(spawn);
		spawn.transferEnergy(creep);
	}
	
	if(target)
	{
		creep.moveTo(target);
		creep.attack(target);
	}
};