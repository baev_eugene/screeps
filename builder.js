/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('builder'); // -> 'a thing'
 */
module.exports = function(creep)
{
    var targets = creep.room.find(Game.CONSTRUCTION_SITES);
    
    /*if (targets.length === 0)/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('builder'); // -> 'a thing'
 */
module.exports = function(creep)
{
    var targets = creep.room.find(Game.CONSTRUCTION_SITES);
    
    if (!targets.length)
    {
        if (creep.energy < creep.energyCapacity)
        {
            var source = creep.room.find(Game.SOURCES);
            creep.moveTo(source[0]);
            creep.harvest(source[0]);
        }
        else
        {
            creep.moveTo(Game.spawns.Spawn1);
            creep.transferEnergy(Game.spawns.Spawn1);
        }
        
        return;
    }
    
    if (creep.energy === 0)
    {
        creep.moveTo(Game.spawns.Spawn1);
        Game.spawns.Spawn1.transferEnergy(creep);
    }
    else
    {
        if (targets.length)
        {
            creep.moveTo(targets[0]);
            creep.build(targets[0]);
        }
    }
};
    {
        console.log("No targets");
        if (creep.energy < creep.energyCapacity)
        {
            var source = creep.room.find(Game.SOURCES);
            creep.moveTo(source[0]);
            creep.harvest(source[0]);
        }
        else
        {
            creep.moveTo(Game.spawns.Spawn1);
            creep.transferEnergy(Game.spawns.Spawn1);
        }
        
        return;
    }*/
    
    if (creep.energy === 0)
    {
        creep.moveTo(Game.spawns.Spawn1);
        Game.spawns.Spawn1.transferEnergy(creep);
    }
    else
    {
        if (targets.length)
        {
            creep.moveTo(targets[0]);
            creep.build(targets[0]);
        }
    }
};